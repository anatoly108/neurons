﻿using System;
using System.Drawing;
using System.IO;

using Color = System.Drawing.Color;

namespace MnistReader
{
    using System.Drawing.Imaging;

    using Neurons;

    public class Program
    {
        private const string SavePath = "c:\\mnist\\img";
        const int PicturesNumber = 100; // Max = 10000

        private static void Main(string[] args)
        {
            Directory.CreateDirectory(SavePath);

            try
            {
                Console.WriteLine("\nStart\n");
                var ifsLabels =
                    new FileStream(@"C:\mnist\train-labels.idx1-ubyte",
                        FileMode.Open); // test labels
                var ifsImages =
                    new FileStream(@"C:\mnist\train-images.idx3-ubyte",
                        FileMode.Open); // test images

                var brLabels =
                    new BinaryReader(ifsLabels);
                var brImages =
                    new BinaryReader(ifsImages);

                var magic1 = brImages.ReadInt32();
                var numImages = brImages.ReadInt32();
                var numRows = brImages.ReadInt32();
                var numCols = brImages.ReadInt32();

                var magic2 = brLabels.ReadInt32();
                var numLabels = brLabels.ReadInt32();

                var pixels = new byte[28][];
                for (var i = 0; i < pixels.Length; ++i)
                    pixels[i] = new byte[28];

                
                for (var di = 0; di < PicturesNumber; ++di)
                {
                    for (var i = 0; i < 28; ++i)
                    {
                        for (var j = 0; j < 28; ++j)
                        {
                            var b = brImages.ReadByte();
                            pixels[i][j] = (byte) (255 - b);
                        }
                    }

                    var lbl = brLabels.ReadByte();

                    var cutter = new Cutter();
                    var resultPixels = cutter.Cut(pixels);

                    var dImage =
                        new DigitImage(resultPixels, lbl);

                    var bitmap = dImage.ConvertToBitmap(dImage.Pixels);

                    var saveFormat = ImageFormat.Jpeg;
                    var name = dImage.Label + "_" + di + "." + saveFormat;
                    var path = Path.Combine(SavePath, name);

                    bitmap.Save(path);

                    Console.WriteLine(dImage.Label);
                } // each image

                ifsImages.Close();
                brImages.Close();
                ifsLabels.Close();
                brLabels.Close();

                Console.WriteLine("\nEnd\n");
                Console.ReadKey();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                Console.ReadKey();
            }
        } 
    } 

    public class DigitImage
    {
        public readonly byte[][] Pixels;
        public readonly byte Label;

        private readonly int _width;

        private readonly int _height;

        public DigitImage(byte[][] pixels,
            byte label)
        {
            _width = pixels[0].Length;
            _height = pixels.Length;
            Pixels = new byte[_height][];
            for (int i = 0; i < _height; ++i)
            {
                Pixels[i] = new byte[_width];
            }

            for (int i = 0; i < _height; ++i)
                for (int j = 0; j < _width; ++j)
                    Pixels[i][j] = pixels[i][j];

            Label = label;
        }

        public override string ToString()
        {
            string s = "";
            for (int i = 0; i < 28; ++i)
            {
                for (int j = 0; j < 28; ++j)
                {
                    if (Pixels[i][j] == 0)
                        s += " "; // white
                    else if (Pixels[i][j] == 255)
                        s += "O"; // black
                    else
                        s += "."; // gray
                }
                s += "\n";
            }
            s += Label.ToString();
            return s;
        } // ToString

        public Bitmap ConvertToBitmap(byte[][] data)
        {
            var bitmap = new Bitmap(_width, _height);

            for (var i = 0; i < _height; ++i)
            {
                for (var j = 0; j < _width; ++j)
                {
                    var value = data[i][j];
                    var color = Color.FromArgb(value, value, value);
                    bitmap.SetPixel(j, i, color);
                }
            }

            return bitmap;
        }
        

    }
}