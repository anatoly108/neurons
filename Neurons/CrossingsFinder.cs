﻿using System.Collections.Generic;
using System.Drawing;

namespace Neurons
{
    using System;

    public class CrossingsFinder
    {
        public IEnumerable<double> Handle(byte[][] pixels)
        {
            var width = pixels.GetLength(0);
            var height = pixels[0].Length;

            var horizontalQuant = width / Constants.HorisontalPointsNumber;
            var verticalQuant = height / Constants.VerticalPointsNumber;

            var horizontalValues = new double[Constants.HorisontalPointsNumber];
            var verticalValues = new double[Constants.VerticalPointsNumber];
            for (var i = 0; i < Constants.HorisontalPointsNumber; i++)
            {
                var colorValue = 255;
                var y = 0;
                var x = i * horizontalQuant;
                while (colorValue > Constants.ColorThreshold)
                {
                    var pixel = pixels[x][y];//[bitmap.GetPixel(x, y);
                    colorValue = pixel;
                    y++;
                }
                // Добавляем не сам индекс пикселя, а отношение длины
                horizontalValues[i] = width/y;
            }

            for (var i = 0; i < Constants.VerticalPointsNumber; i++)
            {
                var colorValue = 255;
                var x = 0;
                var y = i * verticalQuant;
                while (colorValue > Constants.ColorThreshold)
                {
                    var pixel = pixels[x][y];//[bitmap.GetPixel(x, y);
                    colorValue = pixel;
                    x++;
                }
                verticalValues[i] = height/x;
            }

            var values = new List<double>();
            values.AddRange(horizontalValues);
            values.AddRange(verticalValues);

            return values;
        }
    }
}