namespace Neurons
{
    public class Cutter
    {
        public byte[][] Cut(byte[][] pixels)
        {
            var leftToCut = 0;
            var topToCut = 0;
            var rightToCut = 0;
            var bottomToCut = 0;

            var verticalLength = pixels.GetLength(0);
            var horizontaLength = pixels[0].Length;
            var isFound = false;

            for (var i = 0; i < verticalLength; i++)
            {
                for (var j = 0; j < horizontaLength; j++)
                {
                    var pixel = pixels[i][j];
                    if (pixel < Constants.ColorThreshold)
                    {
                        topToCut = i;
                        isFound = true;
                        break;
                    }
                }
                if (isFound) break;
            }

            isFound = false;
            for (var j = 0; j < horizontaLength; j++)
            {
                for (var i = 0; i < verticalLength; i++)
                {
                    var pixel = pixels[i][j];
                    if (pixel < Constants.ColorThreshold)
                    {
                        leftToCut = j;
                        isFound = true;
                        break;
                    }
                }
                if (isFound) break;
            }

            isFound = false;
            for (var i = verticalLength - 1; i >= 0; i--)
            {
                for (var j = 0; j < horizontaLength; j++)
                {
                    var pixel = pixels[i][j];
                    if (pixel < Constants.ColorThreshold)
                    {
                        bottomToCut = i;
                        isFound = true;
                        break;
                    }
                }
                if (isFound) break;
            }

            bottomToCut = verticalLength - 1 - bottomToCut;

            isFound = false;
            for (var j = horizontaLength - 1; j >= 0; j--)
            {
                for (var i = 0; i < verticalLength; i++)
                {
                    var pixel = pixels[i][j];
                    if (pixel < Constants.ColorThreshold)
                    {
                        rightToCut = j;
                        isFound = true;
                        break;
                    }
                }
                if (isFound) break;
            }

            rightToCut = horizontaLength - 1 - rightToCut;

            var oldVerticalLength = verticalLength;
            var newVericalLength = oldVerticalLength - topToCut - bottomToCut;
            var result = new byte[newVericalLength][];

            var oldHorizontalLength = horizontaLength;
            var newHorizontalLength = oldHorizontalLength - leftToCut - rightToCut;

            for (var i = 0; i < result.GetLength(0); i++)
            {
                result[i] = new byte[newHorizontalLength];
            }

            var bottomLimit = verticalLength - bottomToCut;
            var rightLimit = horizontaLength - rightToCut;
            for (var i = topToCut; i < bottomLimit; i++)
            {
                var y = i - topToCut;
                for (var j = leftToCut; j < rightLimit; j++)
                {
                    var x = j - leftToCut;
                    result[y][x] = pixels[i][j];
                }
            }

            return result;
        }
    }
}