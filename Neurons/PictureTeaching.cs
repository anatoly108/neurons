﻿using System.Linq;
using Neurons.KohonenNetwork;

namespace Neurons
{
    public class PictureTeaching
    {
        private readonly CrossingsFinder _crossingsFinder;
        private readonly Teaching _teaching;

        private readonly BitmapProcessor _bitmapProcessor;

        private readonly Cutter _cutter;

        public PictureTeaching(Teaching teaching)
        {
            _teaching = teaching;
            _bitmapProcessor = new BitmapProcessor();
            _crossingsFinder = new CrossingsFinder();
            _cutter = new Cutter();
        }

        public void Teach(string pathToPicture, string correctAnswer, OneLayerNetwork oneLayerNetwork)
        {
            var pixels = _bitmapProcessor.Handle(pathToPicture);
            var cutted = _cutter.Cut(pixels);
            var inputs = _crossingsFinder.Handle(cutted).ToArray();

            _teaching.Teach(inputs, correctAnswer, oneLayerNetwork);
        }
    }
}