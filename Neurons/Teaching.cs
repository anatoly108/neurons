﻿using System.Linq;
using Neurons.KohonenNetwork;

namespace Neurons
{
    public class Teaching
    {
        public void Teach(double[] inputs, string correctAnswer, OneLayerNetwork oneLayerNetwork)
        {
            var neurons = oneLayerNetwork.HiddenLayer.Neurons.Select(x => x.Value);
            var neuron = neurons.FirstOrDefault(x => x.Name == correctAnswer);
            for (var i = 0; i < Constants.InputNeuronsNumber; i++)
            {
                var link = neuron.IncomingLinks[i];
                var input = inputs[i];

                link.Weight += Constants.Eta * (input - link.Weight);
            }
        }
    }
}