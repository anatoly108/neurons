﻿using System;
using System.Linq;

namespace Neurons.KohonenNetwork
{
    public class KohonenNetworkForNumbersFactory
    {
        public KohonenNetwork Create(string networkName)
        {
            var random = new Random();

            var inputNeurons = new Neuron[Constants.InputNeuronsNumber];
            for (var i = 0; i < Constants.InputNeuronsNumber; i++)
            {
                inputNeurons[i] = new Neuron("x" + i, new Link[0]);
            }

            var links = new Link[Constants.HiddenNeuronsNumber][];
            for (var i = 0; i < Constants.HiddenNeuronsNumber; i++)
            {
                links[i] = new Link[Constants.InputNeuronsNumber];

                for (var j = 0; j < Constants.InputNeuronsNumber; j++)
                {
                    links[i][j] = new Link("w" + i + "" + j, inputNeurons[j].Name, random.Next(10));
                }
            }

            var hiddenNeurons = new Neuron[Constants.HiddenNeuronsNumber];
            for (var i = 0; i < Constants.HiddenNeuronsNumber; i++)
            {
                var name = (i + 1).ToString();
                var incomingLinks = links[i];
                hiddenNeurons[i] = new Neuron(name, incomingLinks);
            }

            var inputDictionary = inputNeurons.ToDictionary(x => x.Name, x => x);
            var hiddenDictionary = hiddenNeurons.ToDictionary(x => x.Name, x => x);

            var inputLayer = new Layer(inputDictionary);
            var hiddenLayer = new Layer(hiddenDictionary);

            var network = new KohonenNetwork(networkName, inputLayer, hiddenLayer);

            return network;
        } 
    }
}