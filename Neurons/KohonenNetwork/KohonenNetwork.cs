﻿using System;
using System.Linq;

namespace Neurons.KohonenNetwork
{
    public class KohonenNetwork : OneLayerNetwork
    {
        public KohonenNetwork(string networkName, Layer inputLayer, Layer hiddenLayer) : base(networkName, inputLayer, hiddenLayer)
        {
        }

        public override double Handle(double[] input)
        {
            var i = 0;
            foreach (var neuronKeyValuePair in InputLayer.Neurons)
            {
                neuronKeyValuePair.Value.Power = input[i];
                i++;
            }

            foreach (var neuron in HiddenLayer.Neurons)
            {
                foreach (var incomingLink in neuron.Value.IncomingLinks)
                {
                    Neuron prevNeuron;
                    InputLayer.Neurons.TryGetValue(incomingLink.FromNeuronName, out prevNeuron);
                    if(prevNeuron == null)
                        throw new Exception(string.Format("Не найден нейрон с именем {0}", incomingLink.FromNeuronName));

                    neuron.Value.Power += incomingLink.Weight * prevNeuron.Power;
                }
            }

            var max = HiddenLayer.Neurons.OrderByDescending(x => x.Value.Power).First();

            return double.Parse(max.Value.Name);
        }
    }
}
