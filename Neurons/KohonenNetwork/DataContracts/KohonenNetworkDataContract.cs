﻿using System.Runtime.Serialization;

namespace Neurons.KohonenNetwork.DataContracts
{
    [DataContract(Name = "KohonenNetwork", Namespace = "")]
    public class KohonenNetworkDataContract
    {
        [DataMember]
        public string NetworkName { get; set; }
        [DataMember(Name = "InputLayer")]
        public LayerDataContract InputLayer { get; set; }
        [DataMember(Name = "HiddenLayer")]
        public LayerDataContract hiddenLayer;
    }

    [DataContract(Name = "Layer")]
    public class LayerDataContract
    {
        [DataMember(Name="Neurons")]
        public NeuronDataContract[] Neurons;
    }

    [DataContract(Name = "Neuron")]
    public class NeuronDataContract
    {
        [DataMember(Name = "Name")]
        public string Name { get; set; }
        [DataMember(Name = "Power")]
        public double Value { get; set; }
        [DataMember(Name = "IncomingLinks")]
        public LinkDataContract[] IncomingLinks { get; set; }
    }

    [DataContract(Name = "Link")]
    public class LinkDataContract
    {
        [DataMember]
        public string Name { get; set; }
        [DataMember]
        public double Weight { get; set; }
        [DataMember]
        public string FromNeuron { get; set; }
    }
}