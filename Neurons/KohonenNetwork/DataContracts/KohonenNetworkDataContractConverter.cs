﻿using System.Collections.Generic;

namespace Neurons.KohonenNetwork.DataContracts
{
    public class KohonenNetworkDataContractConverter
    {
        public Neurons.KohonenNetwork.KohonenNetwork Convert(KohonenNetworkDataContract networkDataContract)
        {
            var inputNeurons = new Dictionary<string, Neuron>();
            foreach (var neuronDataContract in networkDataContract.InputLayer.Neurons)
            {
                var incomingLinks = new List<Link>();

                foreach (var linkDataContract in neuronDataContract.IncomingLinks)
                {
                    var link = new Link(linkDataContract.Name, linkDataContract.FromNeuron, linkDataContract.Weight);
                    incomingLinks.Add(link);
                }

                var neuron = new Neuron(neuronDataContract.Name, incomingLinks.ToArray());
                inputNeurons.Add(neuron.Name, neuron);
            }
            var inputLayer = new Layer(inputNeurons);

            var hiddenNeurons = new Dictionary<string, Neuron>();
            foreach (var neuronDataContract in networkDataContract.hiddenLayer.Neurons)
            {
                var incomingLinks = new List<Link>();

                foreach (var linkDataContract in neuronDataContract.IncomingLinks)
                {
                    var link = new Link(linkDataContract.Name, linkDataContract.FromNeuron, linkDataContract.Weight);
                    incomingLinks.Add(link);
                }

                var neuron = new Neuron(neuronDataContract.Name, incomingLinks.ToArray()){Power = neuronDataContract.Value};
                hiddenNeurons.Add(neuron.Name, neuron);
            }
            var hiddenLayer = new Layer(hiddenNeurons);

            var network = new Neurons.KohonenNetwork.KohonenNetwork(networkDataContract.NetworkName, inputLayer, hiddenLayer);

            return network;
        }

        public KohonenNetworkDataContract Convert(Neurons.KohonenNetwork.KohonenNetwork network)
        {
            var hiddenNeurons = new List<NeuronDataContract>();
            foreach (var neuron in network.HiddenLayer.Neurons)
            {
                var incomingLinks = new List<LinkDataContract>();

                foreach (var link in neuron.Value.IncomingLinks)
                {
                    var linkDataContract = new LinkDataContract()
                                               {
                                                   FromNeuron = link.FromNeuronName,
                                                   Name = link.Name,
                                                   Weight = link.Weight
                                               };
                    incomingLinks.Add(linkDataContract);
                }

                var neuronDataContract = new NeuronDataContract() { Name = neuron.Value.Name, IncomingLinks = incomingLinks.ToArray(), Value = neuron.Value.Power };
                hiddenNeurons.Add(neuronDataContract);
            }
            var hiddenLayer = new LayerDataContract() { Neurons = hiddenNeurons.ToArray() };

            var inputNeurons = new List<NeuronDataContract>();
            foreach (var neuron in network.InputLayer.Neurons)
            {
                var incomingLinks = new List<LinkDataContract>();
                if (neuron.Value.IncomingLinks.Length > 0)

                foreach (var link in neuron.Value.IncomingLinks)
                {
                    var linkDataContract = new LinkDataContract()
                    {
                        FromNeuron = link.FromNeuronName,
                        Name = link.Name,
                        Weight = link.Weight
                    };
                    incomingLinks.Add(linkDataContract);
                }

                var neuronDataContract = new NeuronDataContract() { Name = neuron.Value.Name, IncomingLinks = incomingLinks.ToArray(), Value = neuron.Value.Power};
                inputNeurons.Add(neuronDataContract);
            }
            var inputLayer= new LayerDataContract() { Neurons = inputNeurons.ToArray() };

            var networkDataContract = new KohonenNetworkDataContract()
                                          {
                                              NetworkName = network.NetworkName,
                                              hiddenLayer = hiddenLayer,
                                              InputLayer = inputLayer
                                          };
            return networkDataContract;
        }
    }
}