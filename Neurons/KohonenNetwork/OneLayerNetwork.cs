﻿namespace Neurons.KohonenNetwork
{
    public abstract class OneLayerNetwork
    {
        public string NetworkName { get; set; }
        public Layer InputLayer { get; private set; }
        public Layer HiddenLayer { get; private set; }

        protected OneLayerNetwork(string networkName, Layer inputLayer, Layer hiddenLayer)
        {
            NetworkName = networkName;
            InputLayer = inputLayer;
            HiddenLayer = hiddenLayer;
        }

        public abstract double Handle(double[] input);
    }
}