﻿namespace Neurons
{
    using System.Collections.Generic;

    public class Layer
    {
        public readonly Dictionary<string, Neuron> Neurons;

        public Layer(Dictionary<string, Neuron> neurons)
        {
            Neurons = neurons;
        }
    }
}