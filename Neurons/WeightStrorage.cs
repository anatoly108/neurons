﻿using System;
using System.Collections.Generic;
using System.IO;

namespace Neurons
{
    public class WeightStrorage
    {
        private const string FilePath = "weights.txt";
        private const char Separator = ';';

        public void Save(Link[] links)
        {
            if (links.Length > Constants.InputNeuronsNumber * Constants.HiddenNeuronsNumber)
                throw new Exception("Неправильное количество весов");

            var resultLines = new List<string>();
            foreach (var link in links)
            {
                var resultStr = link.Name + Separator + link.Weight + Separator + link.FromNeuronName;
                resultLines.Add(resultStr);
            }
            File.WriteAllLines(FilePath, resultLines);
        }

        public void Read()
        {
            var lines = File.ReadAllLines(FilePath);
            var links = new List<Link>();
            foreach (var line in lines)
            {
                if(line == "")
                    continue;
                var parts = line.Split(Separator);
                var name = parts[0];
                var weight = parts[1];
                var neuronName = parts[2];
//                var link = new Link(name, neuronName, weight);
                /*
                 * 1) Сделать конструктор Link без FromNeuronName, чтобы инициализировать его позже (куда тогда девать neuronName?)
                 * 2) Сделать отдельную структуру для хранения пары имя-вес (Dictionary?)
                 */
            }
        }
    }
}