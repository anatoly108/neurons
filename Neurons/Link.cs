﻿namespace Neurons
{
    public class Link
    {
        public string Name { get; private set; }
        public double PreviousWeight { get; set; }

        public double Weight
        {
            get { return _weight; }
            set
            {
                PreviousWeight = Weight;
                _weight = value;
            }
        }

        private double _weight;
        public string FromNeuronName { get; private set; }

        public Link(string name, string fromNeuronName, double weight)
        {
            Name = name;
            FromNeuronName = fromNeuronName;
            Weight = weight;
            PreviousWeight = weight;
        }
    }
}