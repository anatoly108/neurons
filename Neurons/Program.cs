﻿using System;
using System.Linq;
using Neurons.KohonenNetwork;
using Neurons.KohonenNetwork.DataContracts;

namespace Neurons
{
    using System.IO;
    using System.Runtime.Serialization;

    class Program
    {
        static void Main(string[] args)
        {
            var kohonenNetworkFactory = new KohonenNetworkForNumbersFactory();
            var network = kohonenNetworkFactory.Create("testNetwork");

            // Обучаем

            var teaching = new Teaching();
            var pictureTeaching = new PictureTeaching(teaching);

            pictureTeaching.Teach("TrainingSet/two.jpg", "2", network);
            
            // Теперь пробуем распознать

            var bitmapProcessor = new BitmapProcessor();
            var crossingsFinder = new CrossingsFinder();
            var pixels = bitmapProcessor.Handle("TrainingSet/two.jpg");
            var inputFromPicture = crossingsFinder.Handle(pixels).ToArray();

            var result = network.Handle(inputFromPicture);
            Console.WriteLine(result);

            // Сериализуем
            var serializer = new DataContractSerializer(typeof(KohonenNetworkDataContract));
            var converter = new KohonenNetworkDataContractConverter();
            var networkDataContract = converter.Convert(network);
            using (var stream = new FileStream("network.xml", FileMode.Create))
            {
                serializer.WriteObject(stream, networkDataContract);
            }
            
            // Десериализуем
            KohonenNetworkDataContract deserNetwork;
            using (var stream = new FileStream("network.xml", FileMode.Open))
            {
                deserNetwork = (KohonenNetworkDataContract) serializer.ReadObject(stream);
            }
            network = converter.Convert(deserNetwork);

            result = network.Handle(inputFromPicture);
            Console.WriteLine(result);

            Console.ReadKey();
        }
    }
}