﻿namespace Neurons
{
    public static class Constants
    {
        public const int HorisontalPointsNumber = 5;
        public const int VerticalPointsNumber = 10;

        public const int InputNeuronsNumber = HorisontalPointsNumber + VerticalPointsNumber;
        public const int HiddenNeuronsNumber = 9;

        public const int ColorThreshold = 60;

        public const double Eta = 0.5;
    }
}