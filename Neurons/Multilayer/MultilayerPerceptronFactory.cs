﻿using System;
using System.Collections.Generic;

namespace Neurons.Multilayer
{
    public class MultilayerPerceptronFactory
    {
        public MultilayerPerceptron CreateThreelayer(int inputNum, int hiddenNum, int outputNum)
        {
            var inputLayer = CreateLayer(inputNum, null, "", "in");
            var hiddenLayer = CreateLayer(hiddenNum, inputLayer, "wh", "hn");
            var outputLayer = CreateLayer(outputNum, hiddenLayer, "wo", "on");

            var activationFunction = new SigmoidFunction();
            var layers = new[]{inputLayer, hiddenLayer, outputLayer};

            var perceptron = new MultilayerPerceptron(layers, activationFunction);

            return perceptron;
        }

        private Layer CreateLayer(int neuronsNumber, Layer prevLayer, string linkNamePrefix, string neuronNamePrefix)
        {
            var random = new Random();
            var neurons = new Dictionary<string, Neuron>();
            var layer = new Layer(neurons);

            for (var i = 0; i < neuronsNumber; i++)
            {
                var incomingLinks = new List<Link>();

                if (prevLayer != null)
                {
                    var j = 0;
                    foreach (var inputNeuronKeyValuePair in prevLayer.Neurons)
                    {
                        var inputNeuronName = inputNeuronKeyValuePair.Value.Name;
                        var linkName = string.Format("{0}{1}{2}", linkNamePrefix, i, j);
                        var weight = random.NextDouble();

                        var link = new Link(linkName, inputNeuronName, weight);
                        incomingLinks.Add(link);
                        j++;
                    }
                }

                var neuron = new Neuron(string.Format("{0}{1}", neuronNamePrefix, i), incomingLinks.ToArray());
                neurons.Add(neuron.Name, neuron);
            }

            return layer;
        }
    }
}