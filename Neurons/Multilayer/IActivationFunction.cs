﻿namespace Neurons.Multilayer
{
    public interface IActivationFunction
    {
        double Handle(double summ);
        double Derivative(double summ);
    }
}