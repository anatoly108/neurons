﻿using System;

namespace Neurons.Multilayer
{
    public class SigmoidFunction : IActivationFunction
    {
        public double Handle(double summ)
        {
            var y = 1/(1 + Math.Exp(-summ));

            return y;
        }

        public double Derivative(double summ)
        {
            var ySharp = Handle(summ)*(1 - Handle(summ));

            return ySharp;
        }
    }
}