﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Neurons.Multilayer
{
    public class Backpropagation
    {
        // Допустимая ошибка
        private const double Delta = 0.001;

        private const double Alpha = 0.0001;
        private const double Eta = 0.25;

        
        private int _desiredIndex;

        private readonly GradientCalculator _gradientCalculator;

        public Backpropagation()
        {
            _gradientCalculator = new GradientCalculator();
        }

        public void Handle(MultilayerPerceptron multilayerPerceptron, double[] input, double[] desiredOutputs)
        {
            
            var outputLayerIndex = multilayerPerceptron.Layers.Length - 1;

            var outputLayer = multilayerPerceptron.Layers[multilayerPerceptron.Layers.Length - 1];
            if(desiredOutputs.Length != outputLayer.Neurons.Count)
                throw new Exception("Кол-во desiredOutputs не совпадает с кол-вом нейронов в последнем слое");

            multilayerPerceptron.Handle(input);
            var error = CalculateError(outputLayer.Neurons.Values.ToArray(), desiredOutputs);

            while (error > Delta)
            {
                _desiredIndex = 0;
                _gradientCalculator.Gradients.Clear();
                for (var i = outputLayerIndex; i >= 1; i--)
                {
                    var layer = multilayerPerceptron.Layers[i];
                    foreach (var neuronKeyValuePair in layer.Neurons)
                    {
                        var neuron = neuronKeyValuePair.Value;
                        double gradient;
                        if (i == outputLayerIndex)
                        {
                            var desiredOutput = desiredOutputs[_desiredIndex];
                            _desiredIndex++;
                            gradient = _gradientCalculator.CalculateGradient(neuron, multilayerPerceptron.ActivationFunction, desiredOutput);
                        }
                        else
                        {
                            var nextLayer = multilayerPerceptron.Layers[i + 1];
                            gradient = _gradientCalculator.CalculateGradient(neuron, multilayerPerceptron.ActivationFunction, nextLayer);
                        }

                        var prevLayer = multilayerPerceptron.Layers[i - 1];

                        foreach (var incomingLink in neuron.IncomingLinks)
                        {
                            var wCur = incomingLink.Weight;
                            var wPrev = incomingLink.PreviousWeight;
                            
                            Neuron fromNeuron;
                            prevLayer.Neurons.TryGetValue(incomingLink.FromNeuronName, out fromNeuron);

                            if(fromNeuron == null)
                                throw new Exception("Не найден нейрон " + incomingLink.FromNeuronName);
                            var fromNeuronPower = fromNeuron.Power;
                            
                            incomingLink.Weight = wCur + Alpha*wPrev + Eta*gradient*fromNeuronPower;
                        }
                    }
                }
                
                multilayerPerceptron.Handle(input);
                error = CalculateError(outputLayer.Neurons.Values.ToArray(), desiredOutputs);
            }

        }

        private double CalculateError(Neuron[] outputNeurons, double[] desiredValues)
        {
            double error = 0;

            for(var i = 0; i < outputNeurons.Length; i++)
            {
                var neuron = outputNeurons[i];
                var desiredValue = desiredValues[i];

                var localError = desiredValue - neuron.Power;
                var powedError = Math.Pow(localError, 2);

                error += powedError;
            }

            return error;
        }
    }
}