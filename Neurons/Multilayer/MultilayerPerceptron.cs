﻿using System;
using System.Linq;

namespace Neurons.Multilayer
{
    public class MultilayerPerceptron
    {
    	// TODO: Сделать softmax для multiclass

        public Layer[] Layers { get; private set; }
        public IActivationFunction ActivationFunction { get; private set; }

        public MultilayerPerceptron(Layer[] layers, IActivationFunction activationFunction)
        {
            Layers = layers;
            ActivationFunction = activationFunction;
        }

        public void Handle(double[] input)
        {
            var inputLayer = Layers[0];

            if (input.Length != inputLayer.Neurons.Count)
                throw new Exception("Кол-во входных данных не соответствует числу входных нейронов");

            var i = 0;
            foreach (var neuronKeyValuePair in inputLayer.Neurons)
            {
                neuronKeyValuePair.Value.Power = input[i];
                i++;
            }

            for (i = 1; i < Layers.Length; i++)
            {
                var layer = Layers[i];
                foreach (var neuronKeyValuePair in layer.Neurons)
                {
                    var neuron = neuronKeyValuePair.Value;
                    var prevLayer = Layers[i - 1];

                    ActivateNeuron(neuron, prevLayer);
                }
            }
        }

        public double[] GetOutput()
        {
            var outputLayer = Layers[Layers.Length - 1];

            var outputs = outputLayer.Neurons.Select(x => x.Value.Power);

            return outputs.ToArray();
        }

        private void ActivateNeuron(Neuron neuron, Layer prevLayer)
        {
            double summ = 0;
            foreach (var incomingLink in neuron.IncomingLinks)
            {
                Neuron fromNeuron;
                prevLayer.Neurons.TryGetValue(incomingLink.FromNeuronName, out fromNeuron);

                if (fromNeuron == null)
                    throw new Exception("Не найден нейрон " + incomingLink.FromNeuronName);

                summ += fromNeuron.Power * incomingLink.Weight;
            }

            neuron.Summ = summ;
            neuron.Power = ActivationFunction.Handle(summ);
        }
    }
}