﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Neurons.Multilayer
{
    public class GradientCalculator
    {
        public Dictionary<string, double> Gradients { get; private set; }

        public GradientCalculator()
        {
            Gradients = new Dictionary<string, double>();
        }

        public double CalculateGradient(Neuron neuron, IActivationFunction activationFunction, double desiredOutput)
        {
            var gradient = activationFunction.Derivative(neuron.Summ) * (desiredOutput - neuron.Power);

            // TODO: Как обновлять значения?
            Gradients.Add(neuron.Name, gradient);

            return gradient;
        }

        public double CalculateGradient(Neuron neuron, IActivationFunction activationFunction, Layer nextLayer)
        {
            var gradSumm = 0.0;

            foreach (var nextLayerNeuronKeyValuePair in nextLayer.Neurons)
            {
                var nextLayerNeuron = nextLayerNeuronKeyValuePair.Value;

                var link = GetLink(neuron, nextLayerNeuron);
                if (link == null) continue;

                var nextLayerNeuronGradient = 0.0;
                if(!Gradients.TryGetValue(nextLayerNeuron.Name, out nextLayerNeuronGradient))
                    throw new Exception("Не найден градиент для " + nextLayerNeuron.Name);

                gradSumm += link.Weight * nextLayerNeuronGradient;
            }

            var gradient = activationFunction.Derivative(neuron.Summ) * gradSumm;

            Gradients.Add(neuron.Name, gradient);

            return gradient;
        }

        private Link GetLink(Neuron neuron, Neuron nextLayerNeuron)
        {
            var link = nextLayerNeuron.IncomingLinks.FirstOrDefault(x => x.FromNeuronName == neuron.Name);

            return link;
        }
    }
}