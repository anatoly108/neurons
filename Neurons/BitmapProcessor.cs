﻿namespace Neurons
{
    using System.Drawing;

    public class BitmapProcessor
    {
        public byte[][] Handle(string pathToImage)
        {
            var bitmap = new Bitmap(pathToImage);
            var result = new byte[bitmap.Width][];

            for (var i = 0; i < bitmap.Width; i++)
            {
                result[i] = new byte[bitmap.Height];

                for (var j = 0; j < bitmap.Height; j++)
                {
                    result[i][j] = bitmap.GetPixel(i, j).R;
                }
            }

            return result;
        }
    }
}