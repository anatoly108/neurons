﻿namespace Neurons
{
    public class Neuron
    {
        public string Name { get; private set; }
        public double Power { get; set; }
        public double Summ { get; set; }
        public Link[] IncomingLinks { get; private set; }

        public Neuron(string name, Link[] incomingLinks)
        {
            Name = name;
            IncomingLinks = incomingLinks;
        }
    }
}