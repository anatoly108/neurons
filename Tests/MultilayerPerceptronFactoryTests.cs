﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Neurons.Multilayer;

namespace Tests
{
    [TestClass]
    public class MultilayerPerceptronFactoryTests
    {
        [TestMethod]
        public void ThreeLayerTest()
        {
            var target = new MultilayerPerceptronFactory();

            var actual = target.CreateThreelayer(5, 5, 1);

            Assert.IsTrue(actual.Layers.Length == 3);
        }
        // TODO: Тест на уникальность всех имён; тест, что везде действительно нужное количество нейронов; тест связей
    }
}