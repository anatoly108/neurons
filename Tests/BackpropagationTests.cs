﻿using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Neurons;
using Neurons.Multilayer;

namespace Tests
{
    [TestClass]
    public class BackpropagationTests
    {
        [TestMethod]
        public void LiteFunctionalTest()
        {
            var target = new Backpropagation();
            var factory = new MultilayerPerceptronFactory();
            var multilayerPerceptron = factory.CreateThreelayer(2, 2, 1);

            var input1 = new[] {0.1, 15.6};
            var desiredOutputs = new[]{0.1};
            target.Handle(multilayerPerceptron, input1, desiredOutputs);

            var input2 = new[] { 0.1, 1 };
            desiredOutputs = new[] { 0.2 };
            target.Handle(multilayerPerceptron, input2, desiredOutputs);
        }
        [TestMethod]
        public void FunctionalTest1()
        {
            var target = new Backpropagation();
            var factory = new MultilayerPerceptronFactory();
            var multilayerPerceptron = factory.CreateThreelayer(15, 2, 1);

            var bitmapProcessor = new BitmapProcessor();
            var crossingsFinder = new CrossingsFinder();
            var pixels = bitmapProcessor.Handle("testImg/two.jpg");
            var inputFromPicture = crossingsFinder.Handle(pixels).ToArray();

            target.Handle(multilayerPerceptron, inputFromPicture, new[]{0.2});
            var output = multilayerPerceptron.GetOutput();

            Assert.IsTrue(output[0] >= 0.2 && output[0] < 0.3);
        }
        
        [TestMethod]
        public void FunctionalTest2()
        {
            var target = new Backpropagation();
            var factory = new MultilayerPerceptronFactory();
            var multilayerPerceptron = factory.CreateThreelayer(15, 2, 1);

            var bitmapProcessor = new BitmapProcessor();
            var crossingsFinder = new CrossingsFinder();
            var pixels = bitmapProcessor.Handle("testImg\\4_2.Jpeg");
            var inputFromPicture = crossingsFinder.Handle(pixels).ToArray();

            target.Handle(multilayerPerceptron, inputFromPicture, new[]{0.4});

            pixels = bitmapProcessor.Handle("testImg\\4_53.Jpeg");
            inputFromPicture = crossingsFinder.Handle(pixels).ToArray();

            multilayerPerceptron.Handle(inputFromPicture);

            var output = multilayerPerceptron.GetOutput();

            Assert.IsTrue(output[0] >= 0.4 && output[0] < 0.5);
        }
        [TestMethod]
        public void FunctionalTest3()
        {
            var target = new Backpropagation();
            var factory = new MultilayerPerceptronFactory();
            var multilayerPerceptron = factory.CreateThreelayer(15, 10, 1);

            var inputFromPicture = Helper.GetInputFromPicture("testImg\\4_2.Jpeg");
            target.Handle(multilayerPerceptron, inputFromPicture, new[] { 0.4 });

            inputFromPicture = Helper.GetInputFromPicture("testImg\\0_1.Jpeg");
            target.Handle(multilayerPerceptron, inputFromPicture, new[] { 0.0 });

            inputFromPicture = Helper.GetInputFromPicture("testImg\\1_6.Jpeg");
            target.Handle(multilayerPerceptron, inputFromPicture, new[] { 0.1 });

            inputFromPicture = Helper.GetInputFromPicture("testImg\\2_82.Jpeg");
            target.Handle(multilayerPerceptron, inputFromPicture, new[] { 0.2 });

            inputFromPicture = Helper.GetInputFromPicture("testImg\\4_2.Jpeg");
            target.Handle(multilayerPerceptron, inputFromPicture, new[] { 0.4 });

            inputFromPicture = Helper.GetInputFromPicture("testImg\\4_20.Jpeg");



            multilayerPerceptron.Handle(inputFromPicture);

            var output = multilayerPerceptron.GetOutput();

            Assert.IsTrue(output[0] >= 0.4 && output[0] < 0.5);
        }

    }
        public static class Helper
        {
            public static double[] GetInputFromPicture(string path)
            {
                var bitmapProcessor = new BitmapProcessor();
                var crossingsFinder = new CrossingsFinder();
                var pixels = bitmapProcessor.Handle(path);
                var inputFromPicture = crossingsFinder.Handle(pixels).ToArray();
                return inputFromPicture;
            }
        }
}