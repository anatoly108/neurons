﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Neurons.Multilayer;

namespace Tests
{
    [TestClass]
    public class MultilayerPerceptronTests
    {
        [TestMethod]
        [ExpectedException(typeof(Exception))]
        public void WrongInputCountTest()
        {
            var factory = new MultilayerPerceptronFactory();
            var target = factory.CreateThreelayer(3, 2, 1);

            target.Handle(new double[]{0, 0, 0, 0});
        }
        [TestMethod]
        public void LiteFunctionalTest()
        {
            var factory = new MultilayerPerceptronFactory();
            var target = factory.CreateThreelayer(2, 2, 1);

            target.Handle(new double[] { 0, 0 });
        }

        // TODO: Сделать настраиваемые значения весов, чтобы сделать ожидамый аутпут
    }
}