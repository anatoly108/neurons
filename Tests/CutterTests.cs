﻿namespace Tests
{
    using System.Collections;
    using Microsoft.VisualStudio.TestTools.UnitTesting;

    using Neurons;

    [TestClass]
    public class CutterTests
    {
        [TestMethod]
        public void SquareTest()
        {
            var target = new Cutter();

            var pixels = new byte[6][];
            for (int i = 0; i < 6; i++)
            {
                pixels[i] = new byte[5];
                for (int j = 0; j < 5; j++)
                {
                    pixels[i][j] = 255;
                }
            }

            pixels[2][1] = 0;
            pixels[2][2] = 0;
            pixels[3][1] = 0;
            pixels[3][2] = 0;

            var actual = target.Cut(pixels);

            var expected = new byte[2][];
            for (int i = 0; i < 2; i++)
            {
                expected[i] = new byte[2];
            }

            expected[0][0] = 0;
            expected[0][1] = 0;
            expected[1][0] = 0;
            expected[1][1] = 0;

            CollectionAssert.AreEqual(expected, actual, new PixelsComparer());
        }
    }

    public class PixelsComparer : IComparer
    {
        public int Compare(object x, object y)
        {
            var xs = (byte[])x;
            var ys = (byte[])y;
            if (xs.Length != ys.Length) return 1;

            for (int i = 0; i < xs.Length; i++)
            {
                if (xs[i] != ys[i]) return 1;
            }

            return 0;
        }
    }
}