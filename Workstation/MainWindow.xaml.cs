﻿using System.Windows;

namespace Workstation
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private readonly NetworkViewModel _networkViewModel = new NetworkViewModel();

        public MainWindow()
        {
            InitializeComponent();
            DataContext = _networkViewModel;
        }
    }
}
