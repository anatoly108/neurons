﻿using System.ComponentModel;
using Neurons;
using Neurons.KohonenNetwork;

namespace Workstation
{
    public class NetworkViewModel
    {
        // TODO: ListBox и DataContent?

        private readonly KohonenNetwork _kohonenNetwork;
        public event PropertyChangedEventHandler PropertyChanged;

        public NetworkViewModel()
        {
            var factory = new KohonenNetworkForNumbersFactory();
            _kohonenNetwork = factory.Create("new network");
        }

        public string NetworkName
        {
            get { return _kohonenNetwork.NetworkName; }
            set
            {
                _kohonenNetwork.NetworkName = value;
                RaisePropertyChanged("NetworkName");
            }
        }

        private void RaisePropertyChanged(string propertyName)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(propertyName));
            }
        }
    }
}